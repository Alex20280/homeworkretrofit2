package layouts.sourceit.com.homeworkretrofit2;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static layouts.sourceit.com.homeworkretrofit2.Resource.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Retrofit.getBanks(new Callback<List<Organization>>() {
            @Override
            public void success(List<Organization> org, Response response) {
                Toast.makeText(MainActivity.this, org.get(5).address, Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "%%%%", Toast.LENGTH_LONG).show();
            }
        });
    }
}
