package layouts.sourceit.com.homeworkretrofit2;

import android.provider.ContactsContract;

import java.util.Currency;
import java.util.List;

public class Resource {

    String date;
    List<Organization> organizations;

    public static class Organization{
        String title;
        String address;
        Currencies currencies;
    }
    public static class Currencies{
        SingleCurrency EUR;
        SingleCurrency RUB;
        SingleCurrency USD;
    }

    public static class SingleCurrency{
        String ask;
        String bid;
    }
}
