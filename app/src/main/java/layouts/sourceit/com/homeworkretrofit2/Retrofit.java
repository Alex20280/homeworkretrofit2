package layouts.sourceit.com.homeworkretrofit2;


import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.GET;

public class Retrofit {

    public static final String ENDPOINT = "http://resources.finance.ua";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface{
        @GET("/ru/public/currency-cash.json")
        void getBanks(Callback<List<Resource.Organization>> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getBanks(Callback<List<Resource.Organization>> callback) {
        apiInterface.getBanks(callback);
    }
}
